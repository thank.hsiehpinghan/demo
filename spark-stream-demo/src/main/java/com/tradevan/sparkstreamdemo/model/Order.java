package com.tradevan.sparkstreamdemo.model;

import java.util.List;

public class Order {
	private String customer;
	private List<Product> products;
	private Double latitude;
	private Double longitude;

	public Order() {
		super();
	}

	public Order(String customer, List<Product> products, Double latitude, Double longitude) {
		super();
		this.customer = customer;
		this.products = products;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public static class Product {
		String name;
		Integer price;
		Integer amount;

		public Product() {
			super();
		}

		public Product(String name, Integer price, Integer amount) {
			super();
			this.name = name;
			this.price = price;
			this.amount = amount;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getPrice() {
			return price;
		}

		public void setPrice(Integer price) {
			this.price = price;
		}

		public Integer getAmount() {
			return amount;
		}

		public void setAmount(Integer amount) {
			this.amount = amount;
		}

	}

}

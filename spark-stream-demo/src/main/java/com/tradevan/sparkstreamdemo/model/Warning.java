package com.tradevan.sparkstreamdemo.model;

public class Warning {
	private String time;
	private Double latitude;
	private Double longitude;
	private String product;
	private Double bill;

	public Warning() {
		super();
	}

	public Warning(String time, Double latitude, Double longitude, String product, Double bill) {
		super();
		this.time = time;
		this.latitude = latitude;
		this.longitude = longitude;
		this.product = product;
		this.bill = bill;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Double getBill() {
		return bill;
	}

	public void setBill(Double bill) {
		this.bill = bill;
	}

}

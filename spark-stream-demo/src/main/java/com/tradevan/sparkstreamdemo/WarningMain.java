package com.tradevan.sparkstreamdemo;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.sparkstreamdemo.model.Order;
import com.tradevan.sparkstreamdemo.model.Warning;
import com.tradevan.sparkstreamdemo.producer.LongStringProducer;

import scala.Tuple2;

public class WarningMain {
	@SuppressWarnings("serial")
	public static void main(String[] args) throws Exception {
		SparkConf conf = new SparkConf().setMaster("local").setAppName("warning-stream");
		JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(1));
		Map<String, Object> kafkaParams = new HashMap<>();
		kafkaParams.put("bootstrap.servers", "localhost:9092");
		kafkaParams.put("key.deserializer", LongDeserializer.class);
		kafkaParams.put("value.deserializer", StringDeserializer.class);
		kafkaParams.put("group.id", "warning-stream");
		// kafkaParams.put("auto.offset.reset", "latest");
		kafkaParams.put("auto.offset.reset", "earliest");
		kafkaParams.put("enable.auto.commit", true);
		
		Collection<String> topics = Arrays.asList("order");
		JavaInputDStream<ConsumerRecord<Long, String>> stream = 
				KafkaUtils.createDirectStream(
						streamingContext,
						LocationStrategies.PreferConsistent(), 
						ConsumerStrategies.<Long, String>Subscribe(topics, kafkaParams));
		JavaPairDStream<Long, String> dStream = 
				stream.mapToPair(record -> new Tuple2<>(record.key(), record.value()));
		dStream.foreachRDD(new VoidFunction<JavaPairRDD<Long, String>>() {
			@Override
			public void call(JavaPairRDD<Long, String> rdd) throws Exception {
				LongStringProducer longStringProducer = new LongStringProducer();
				ObjectMapper objectMapper = new ObjectMapper();
				for (Tuple2<Long, String> t : rdd.collect()) {
					Order order = objectMapper.readValue(t._2, Order.class);
					for (Order.Product p : order.getProducts()) {
						Date now = new Date();
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
						String time = simpleDateFormat.format(now);
						Double latitude = order.getLatitude();
						Double longitude = order.getLongitude();
						String product = p.getName();
						Double bill = Double.valueOf(p.getAmount() * p.getPrice());
						Warning warning = new Warning(time, latitude, longitude, product, bill);
						if (warning.getBill() < 300) {
							continue;
						}
						longStringProducer.send(
								"warning", 
								objectMapper.writeValueAsString(warning));
					}
				}
			}
		});
		streamingContext.start();
		streamingContext.awaitTermination();
	}
}

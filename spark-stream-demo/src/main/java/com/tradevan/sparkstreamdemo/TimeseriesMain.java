package com.tradevan.sparkstreamdemo;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.sparkstreamdemo.model.Order;
import com.tradevan.sparkstreamdemo.model.Timeseries;
import com.tradevan.sparkstreamdemo.producer.LongStringProducer;

import scala.Tuple2;

public class TimeseriesMain {
	@SuppressWarnings("serial")
	public static <U> void main(String[] args) throws Exception {
		SparkConf conf = new SparkConf().setMaster("local").setAppName("timeseries-stream");
		JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(1));
		Map<String, Object> kafkaParams = new HashMap<>();
		kafkaParams.put("bootstrap.servers", "localhost:9092");
		kafkaParams.put("key.deserializer", LongDeserializer.class);
		kafkaParams.put("value.deserializer", StringDeserializer.class);
		kafkaParams.put("group.id", "timeseries-stream");
		kafkaParams.put("auto.offset.reset", "earliest");
		kafkaParams.put("enable.auto.commit", true);
		
		Collection<String> topics = Arrays.asList("order");
		JavaInputDStream<ConsumerRecord<Long, String>> stream = 
				KafkaUtils.createDirectStream(streamingContext,
						LocationStrategies.PreferConsistent(), 
						ConsumerStrategies.<Long, String>Subscribe(topics, kafkaParams));
		JavaPairDStream<Long, String> dStream = 
				stream.mapToPair(record -> new Tuple2<>(record.key(), record.value()));
		dStream.mapValues(new Function<String, Timeseries>() {
			ObjectMapper objectMapper = new ObjectMapper();
			@Override
			public Timeseries call(String str) throws Exception {
				Order order = objectMapper.readValue(str, Order.class);
				Timeseries timeseries = Timeseries.builder(order).build();
				return timeseries;
			}
		})
		.reduceByWindow(new Function2<Tuple2<Long,Timeseries>, 
				Tuple2<Long,Timeseries>, Tuple2<Long,Timeseries>>() {
			@Override
			public Tuple2<Long, Timeseries> call(Tuple2<Long, Timeseries> t0, 
					Tuple2<Long, Timeseries> t1) throws Exception {
				return new Tuple2<>(0L, t0._2.add(t1._2));
			}
			
		}, Durations.seconds(5), Durations.seconds(5))
		.foreachRDD(new VoidFunction<JavaRDD<Tuple2<Long,Timeseries>>>() {
			private static final long serialVersionUID = 1L;
			@Override
			public void call(JavaRDD<Tuple2<Long, Timeseries>> rdd) throws Exception {
				LongStringProducer longStringProducer = new LongStringProducer();
				ObjectMapper objectMapper = new ObjectMapper();
				for (Tuple2<Long, Timeseries> t : rdd.collect()) {
					longStringProducer.send(
							"timeseries", 
							objectMapper.writeValueAsString(t._2));
				}
			}
		});
		streamingContext.start();
		streamingContext.awaitTermination();
	}
}

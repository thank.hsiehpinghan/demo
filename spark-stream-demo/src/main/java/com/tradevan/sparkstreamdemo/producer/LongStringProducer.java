package com.tradevan.sparkstreamdemo.producer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class LongStringProducer {
	private final long SLEEP_MILLISECONDS = 100;
	private Producer<Long, String> longStringProducer = getLongStringProducer();

	public RecordMetadata send(String topic, String value) throws InterruptedException, ExecutionException {
		ProducerRecord<Long, String> record = new ProducerRecord<>(topic, value);
		Future<RecordMetadata> future = longStringProducer.send(record);
		while (future.isDone() == false) {
			Thread.sleep(SLEEP_MILLISECONDS);
		}
		return future.get();
	}
	
	private Producer<Long, String> getLongStringProducer() {
		Properties properties = generateLongStringProducerProperties();
		return new KafkaProducer<>(properties);
	}
	
	private Properties generateLongStringProducerProperties() {
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		properties.put(ProducerConfig.RETRIES_CONFIG, 3);
		properties.put(ProducerConfig.BATCH_SIZE_CONFIG, 0);
		properties.put(ProducerConfig.LINGER_MS_CONFIG, 1);
		properties.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 1000);
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		return properties;
	}
}


package com.tradevan.webappdemo.constant;

public class Constant {
	public static final String ENDPOINT = "/endpoint";
	public static final String CUSTOMER_BILL_TOPIC = "/customer_bill_topic";
	public static final String CUSTOMER_BILL_TOPIC_RESPONSE = CUSTOMER_BILL_TOPIC + "/response";
	public static final String PRODUCT_BILL_TOPIC = "/product_bill_topic";
	public static final String PRODUCT_BILL_TOPIC_RESPONSE = PRODUCT_BILL_TOPIC + "/response";
	public static final String TIMESERIES_TOPIC = "/timeseries_topic";
	public static final String TIMESERIES_TOPIC_RESPONSE = TIMESERIES_TOPIC + "/response";
	public static final String MAP_TOPIC = "/map_topic";
	public static final String MAP_TOPIC_RESPONSE = MAP_TOPIC + "/response";
	public static final String WARNING_TOPIC = "/warning_topic";
	public static final String WARNING_TOPIC_RESPONSE = WARNING_TOPIC + "/response";
	public static final long SCHEDULED_DELAY_MILLI_SECOND = 1000;
	public static final long POOL_TIMEOUT_MILLI_SECOND = 1000;
}

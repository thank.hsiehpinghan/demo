package com.tradevan.webappdemo.model;

public class Timeseries {
	private Integer value_0;
	private Integer value_1;

	public Timeseries() {
		super();
	}

	public Timeseries(Integer value_0, Integer value_1) {
		super();
		this.value_0 = value_0;
		this.value_1 = value_1;
	}

	public Integer getValue_0() {
		return value_0;
	}

	public void setValue_0(Integer value_0) {
		this.value_0 = value_0;
	}

	public Integer getValue_1() {
		return value_1;
	}

	public void setValue_1(Integer value_1) {
		this.value_1 = value_1;
	}

}

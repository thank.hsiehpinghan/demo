package com.tradevan.webappdemo.schedule;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.webappdemo.constant.Constant;
import com.tradevan.webappdemo.model.ProductBill;

@Component
public class ProductBillSchedule {
	@Autowired
	private Consumer<Long, String> productBillConsumer;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private ObjectMapper objectMapper;

	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll() throws Exception {
		productBillConsumer.subscribe(Arrays.asList("product_bill"));
		ConsumerRecords<Long, String> records = productBillConsumer.poll(Constant.POOL_TIMEOUT_MILLI_SECOND);
		if(records.isEmpty() == true) {
			return;
		}
		Set<ProductBill> productBills = new HashSet<>();
		for(ConsumerRecord<Long, String> record : records) {
			ProductBill productBill = objectMapper.readValue(record.value(), ProductBill.class);
			productBills.add(productBill);
		}
		template.convertAndSend(Constant.PRODUCT_BILL_TOPIC_RESPONSE, productBills);
	}	

//	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll_test() throws Exception {
		List<ProductBill> productBills = new LinkedList<>();
		for(int i = 0; i < 3; ++i) {
			Integer value = (int)(Math.random() * 10);
			String name = "product_" + i;
			ProductBill productBill = new ProductBill(value, name);
			productBills.add(productBill);
		}
		template.convertAndSend(Constant.PRODUCT_BILL_TOPIC_RESPONSE, productBills);
	}
}

package com.tradevan.webappdemo.schedule;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.webappdemo.constant.Constant;
import com.tradevan.webappdemo.model.CustomerBill;

@Component
public class CustomerBillSchedule {
	@Autowired
	private Consumer<Long, String> customerBillConsumer;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private ObjectMapper objectMapper;

	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll() throws Exception {
		customerBillConsumer.subscribe(Arrays.asList("customer_bill"));
		ConsumerRecords<Long, String> records = customerBillConsumer.poll(Constant.POOL_TIMEOUT_MILLI_SECOND);
		if(records.isEmpty() == true) {
			return;
		}
		Set<CustomerBill> customerBills = new HashSet<>();
		for(ConsumerRecord<Long, String> record : records) {
			CustomerBill customerBill = objectMapper.readValue(record.value(), CustomerBill.class);
			customerBills.add(customerBill);
		}
		template.convertAndSend(Constant.CUSTOMER_BILL_TOPIC_RESPONSE, customerBills);
	}	

//	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll_test() throws Exception {
		List<CustomerBill> customerBills = new LinkedList<>();
		for(int i = 0; i < 3; ++i) {
			Integer value = (int)(Math.random() * 10);
			String name = "customer_" + i;
			CustomerBill customerBill = new CustomerBill(value, name);
			customerBills.add(customerBill);
		}
		template.convertAndSend(Constant.CUSTOMER_BILL_TOPIC_RESPONSE, customerBills);
	}
}

package com.tradevan.webappdemo.schedule;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.webappdemo.constant.Constant;
import com.tradevan.webappdemo.model.Map;

@Component
public class MapSchedule {
	@Autowired
	private Consumer<Long, String> mapConsumer;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private ObjectMapper objectMapper;

	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll() throws Exception {
		mapConsumer.subscribe(Arrays.asList("map"));
		ConsumerRecords<Long, String> records = mapConsumer.poll(Constant.POOL_TIMEOUT_MILLI_SECOND);
		if (records.isEmpty() == true) {
			return;
		}
		List<Map> maps = new LinkedList<>();
		for (ConsumerRecord<Long, String> record : records) {
			Map map = objectMapper.readValue(record.value(), Map.class);
			maps.add(map);
		}
		template.convertAndSend(Constant.MAP_TOPIC_RESPONSE, maps);
	}

//	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll_test() throws Exception {
		List<Map> maps = new LinkedList<>();
		for (int i = 0; i < 3; ++i) {
			Double latitude = 25.06 + 0.01 * i;
			Double longitude = 121.6 + 0.01 * i;
			// Double latitude = 25.0616509;
			// Double longitude = 121.6;
			Map map = new Map(latitude, longitude);
			maps.add(map);
		}
		template.convertAndSend(Constant.MAP_TOPIC_RESPONSE, maps);
	}
}

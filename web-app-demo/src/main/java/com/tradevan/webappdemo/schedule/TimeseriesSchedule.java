package com.tradevan.webappdemo.schedule;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.webappdemo.constant.Constant;
import com.tradevan.webappdemo.model.Timeseries;

@Component
public class TimeseriesSchedule {
	private final Boolean INSERT_FROM_HEAD = false;
	private final Boolean INCREATE_DATA_LENGTH = false;
	@Autowired
	private Consumer<Long, String> timeseriesConsumer;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private ObjectMapper objectMapper;

	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll() throws Exception {
		timeseriesConsumer.subscribe(Arrays.asList("timeseries"));
		ConsumerRecords<Long, String> records = timeseriesConsumer.poll(Constant.POOL_TIMEOUT_MILLI_SECOND);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
		String axisTag = simpleDateFormat.format(new Date());
		List<List<Object>> result = new LinkedList<>();
		if(records.isEmpty() == true) {
			List<Object> data_0 = Arrays.asList(Integer.valueOf(0), 0, INSERT_FROM_HEAD, INCREATE_DATA_LENGTH, axisTag);
			result.add(data_0);
			List<Object> data_1 = Arrays.asList(Integer.valueOf(1), 0, INSERT_FROM_HEAD, INCREATE_DATA_LENGTH, axisTag);
			result.add(data_1);
			template.convertAndSend(Constant.TIMESERIES_TOPIC_RESPONSE, result);
		} else {
			for(ConsumerRecord<Long, String> record : records) {
				Timeseries timeseries = objectMapper.readValue(record.value(), Timeseries.class);
				List<Object> data_0 = Arrays.asList(Integer.valueOf(0), timeseries.getValue_0(), INSERT_FROM_HEAD, INCREATE_DATA_LENGTH, axisTag);
				result.add(data_0);
				List<Object> data_1 = Arrays.asList(Integer.valueOf(1), timeseries.getValue_1(), INSERT_FROM_HEAD, INCREATE_DATA_LENGTH, axisTag);
				result.add(data_1);
				template.convertAndSend(Constant.TIMESERIES_TOPIC_RESPONSE, result);
			}			
		}
	}	

//	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll_test() throws Exception {
		List<List<Object>> timeseriess = new LinkedList<>();
		for(int i = 0; i < 3; ++i) {
			Integer index = i;
			Integer value = (int)(Math.random() * 10);
			String axisTag = "time_" + i;
			List<Object> datas = Arrays.asList(index, value, INSERT_FROM_HEAD, INCREATE_DATA_LENGTH, axisTag);
			timeseriess.add(datas);
		}
		template.convertAndSend(Constant.TIMESERIES_TOPIC_RESPONSE, timeseriess);
	}
}

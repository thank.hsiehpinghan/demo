package com.tradevan.webappdemo.schedule;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.webappdemo.constant.Constant;
import com.tradevan.webappdemo.model.ProductBill;
import com.tradevan.webappdemo.model.Warning;

@Component
public class WarningSchedule {
	@Autowired
	private Consumer<Long, String> warningConsumer;
	@Autowired
	private SimpMessagingTemplate template;
	@Autowired
	private ObjectMapper objectMapper;

	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll() throws Exception {
		warningConsumer.subscribe(Arrays.asList("warning"));
		ConsumerRecords<Long, String> records = warningConsumer.poll(Constant.POOL_TIMEOUT_MILLI_SECOND);
		if(records.isEmpty() == true) {
			return;
		}
		List<Warning> warnings = new LinkedList<>();
		for(ConsumerRecord<Long, String> record : records) {
			Warning warning = objectMapper.readValue(record.value(), Warning.class);
			warnings.add(warning);
		}
		template.convertAndSend(Constant.WARNING_TOPIC_RESPONSE, warnings);
	}	

//	@Scheduled(fixedDelay = Constant.SCHEDULED_DELAY_MILLI_SECOND)
	public void poll_test() throws Exception {
		List<Warning> warnings = new LinkedList<>();
		for(int i = 0; i < 3; ++i) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
			String time = simpleDateFormat.format(new Date());
			Double latitude = 25.06 + 0.01 * i;
			Double longitude = 121.6 + 0.01 * i;
			String product = "prod_" + i;
			Double bill =100 + i * 10.0;
			Warning warning = new Warning(time, latitude, longitude, product, bill);
			warnings.add(warning);
		}
		template.convertAndSend(Constant.WARNING_TOPIC_RESPONSE, warnings);
	}
}

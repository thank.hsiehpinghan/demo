package com.tradevan.kafkastreamdemo.stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tradevan.kafkastreamdemo.model.Order;
import com.tradevan.kafkastreamdemo.util.TestUtil;

/**
 * not work !!!
 */
@SpringBootTest
@ActiveProfiles("window")
@RunWith(SpringRunner.class)
public class WindowStreamTest {

	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private WindowStream windowStream;

	@Test
	public void start() throws Exception {
		Order order = TestUtil.generateOrder(0);
		String jsonStr = objectMapper.writeValueAsString(order);
		System.err.println(jsonStr);
		windowStream.start();
	}
}

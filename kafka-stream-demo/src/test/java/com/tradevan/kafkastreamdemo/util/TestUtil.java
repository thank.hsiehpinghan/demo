package com.tradevan.kafkastreamdemo.util;

import java.util.ArrayList;
import java.util.List;

import com.tradevan.kafkastreamdemo.model.Order;
import com.tradevan.kafkastreamdemo.model.Order.Product;

public class TestUtil {
	private static final int SIZE = 3;

	public static Order generateOrder(int x) {
		String customer = "cust_" + x;
		List<Product> product = generateProducts(x);
		Double latitude = 25.06 + 0.01 * Math.random() * 10;
		Double longitude = 121.6 + 0.01 * Math.random() * 10;
		Order order = new Order(customer, product, latitude, longitude);
		return order;
	}

	private static List<Product> generateProducts(int x) {
		List<Product> products = new ArrayList<Product>(SIZE);
		for (int i = 0; i < SIZE; ++i) {
			Product product = generateProduct(i);
			products.add(product);
		}
		return products;
	}

	private static Product generateProduct(int x) {
		String name = "prod_" + x;
		Integer price = 100 + x * 10;
		Integer amount = 1 + x;
		Product product = new Product(name, price, amount);
		return product;
	}
}

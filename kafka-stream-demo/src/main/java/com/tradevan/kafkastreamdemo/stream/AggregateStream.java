package com.tradevan.kafkastreamdemo.stream;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.TopologyBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.tradevan.kafkastreamdemo.deserializer.JsonDeserializer;
import com.tradevan.kafkastreamdemo.model.CustomerBill;
import com.tradevan.kafkastreamdemo.model.Order;
import com.tradevan.kafkastreamdemo.model.ProductBill;
import com.tradevan.kafkastreamdemo.processor.CustomerBillProcessor;
import com.tradevan.kafkastreamdemo.processor.ProductBillProcessor;
import com.tradevan.kafkastreamdemo.serializer.JsonSerializer;

@Component
@Profile("aggregate")
public class AggregateStream {
	private static final Logger LOGGER = LoggerFactory.getLogger(AggregateStream.class);
	@Value("${bootstrap_servers}")
	private String bootstrapServers;

	public void start() {

		TopologyBuilder topologyBuilder = new TopologyBuilder();
		// @formatter:off
		topologyBuilder
			.addSource("SOURCE", new LongDeserializer(), new JsonDeserializer<>(Order.class), "order")
			.addProcessor("CUSTOMER_BILL_PROCESSOR", CustomerBillProcessor::new, "SOURCE")
				.addStateStore(Stores.create("customer_bill_store").withStringKeys().withValues(getCustomerBillSerde()).inMemory().maxEntries(1000).build(),"CUSTOMER_BILL_PROCESSOR")
			.addSink("CUSTOMER_BILL_SINK", "customer_bill", new LongSerializer(), new JsonSerializer<CustomerBill>(), "CUSTOMER_BILL_PROCESSOR")
			.addProcessor("PRODUCT_BILL_PROCESSOR", ProductBillProcessor::new, "SOURCE")
			.addStateStore(Stores.create("product_bill_store").withStringKeys().withValues(getProductBillSerde()).inMemory().maxEntries(1000).build(),"PRODUCT_BILL_PROCESSOR")
			.addSink("PRODUCT_BILL_SINK", "product_bill", new LongSerializer(), new JsonSerializer<ProductBill>(), "PRODUCT_BILL_PROCESSOR");
		// @formatter:on
		Properties properties = generateProperties();
		KafkaStreams kafkaStreams = new KafkaStreams(topologyBuilder, properties);
		kafkaStreams.setUncaughtExceptionHandler((Thread thread, Throwable throwable) -> {
			LOGGER.error("kafka streams start error !!!", throwable);
			throw new RuntimeException(throwable);
		});
		CountDownLatch countDownLatch = new CountDownLatch(1);
		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread(this.getClass().getName()) {
			@Override
			public void run() {
				kafkaStreams.close();
				countDownLatch.countDown();
			}
		});
		try {
			kafkaStreams.start();
			countDownLatch.await();
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	private Serde<CustomerBill> getCustomerBillSerde() {
		JsonSerializer<CustomerBill> serializer = new JsonSerializer<>();
		JsonDeserializer<CustomerBill> deserializer = new JsonDeserializer<>(CustomerBill.class);
		Serde<CustomerBill> serde = Serdes.serdeFrom(serializer, deserializer);
		return serde;
	}

	private Serde<ProductBill> getProductBillSerde() {
		JsonSerializer<ProductBill> serializer = new JsonSerializer<>();
		JsonDeserializer<ProductBill> deserializer = new JsonDeserializer<>(ProductBill.class);
		Serde<ProductBill> serde = Serdes.serdeFrom(serializer, deserializer);
		return serde;
	}

	private Properties generateProperties() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "aggregate");
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		return properties;
	}
}

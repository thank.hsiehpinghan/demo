package com.tradevan.kafkastreamdemo.stream;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.kstream.internals.WindowedDeserializer;
import org.apache.kafka.streams.kstream.internals.WindowedSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.tradevan.kafkastreamdemo.deserializer.JsonDeserializer;
import com.tradevan.kafkastreamdemo.model.Order;
import com.tradevan.kafkastreamdemo.model.Timeseries;
import com.tradevan.kafkastreamdemo.serializer.JsonSerializer;

@Component
@Profile("window")
public class WindowStream {
	private static final Logger LOGGER = LoggerFactory.getLogger(WindowStream.class);
	@Value("${bootstrap_servers}")
	private String bootstrapServers;

	public void start() {
		KStreamBuilder kStreamBuilder = new KStreamBuilder();
		JsonSerializer<Timeseries> timeseriesSerializer = new JsonSerializer<>();
		JsonDeserializer<Timeseries> timeseriesDeserializer = new JsonDeserializer<>(Timeseries.class);
		Serde<Timeseries> timeseriesSerde = Serdes.serdeFrom(timeseriesSerializer, timeseriesDeserializer);
		WindowedSerializer<Long> windowedSerializer = new WindowedSerializer<>(new LongSerializer());
		WindowedDeserializer<Long> windowedDeserializer = new WindowedDeserializer<>(new LongDeserializer());
		Serde<Windowed<Long>> windowedSerde = Serdes.serdeFrom(windowedSerializer, windowedDeserializer);
		// @formatter:off
		KStream<Long, Order> kStream = kStreamBuilder.stream(
			Serdes.Long(),
			Serdes.serdeFrom(new JsonSerializer<>(), new JsonDeserializer<>(Order.class)),
			"order");
		kStream
			.map((k,v) -> new KeyValue<>(k, Timeseries.builder(v).build()))
			.groupByKey()
			.aggregate(
				Timeseries::new, 
				(k, v, timeseries) -> timeseries.add(v), TimeWindows.of(5 * 1000), timeseriesSerde, "timeseries_store")
			.to(windowedSerde, timeseriesSerde, "timeseries");
		// @formatter:on
		Properties properties = generateProperties();
		KafkaStreams kafkaStreams = new KafkaStreams(kStreamBuilder, properties);
		kafkaStreams.setUncaughtExceptionHandler((Thread thread, Throwable throwable) -> {
			LOGGER.error("start error !!!", throwable);
			throw new RuntimeException(throwable);
		});
		CountDownLatch countDownLatch = new CountDownLatch(1);
		// attach shutdown handler to catch control-c
		Runtime.getRuntime().addShutdownHook(new Thread(this.getClass().getName()) {
			@Override
			public void run() {
				kafkaStreams.close();
				countDownLatch.countDown();
			}
		});
		try {
			kafkaStreams.start();
			countDownLatch.await();
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	private Properties generateProperties() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "window");
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		return properties;
	}

}

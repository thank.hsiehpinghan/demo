package com.tradevan.kafkastreamdemo.model;

import com.tradevan.kafkastreamdemo.model.Order.Product;

public class CustomerBill {
	private Integer value;
	private String name;

	public CustomerBill() {
		super();
	}

	public CustomerBill(Integer value, String name) {
		super();
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public void addValue(Integer value) {
		this.value += value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Builder builder(Order order) {
		return new Builder(order);
	}

	public static final class Builder {
		private Integer value = 0;
		private String name;

		public Builder(Order order) {
			super();
			this.name = order.getCustomer();
			for (Product product : order.getProducts()) {
				value += product.getAmount() * product.getPrice();
			}
		}

		public CustomerBill build() {
			return new CustomerBill(value, name);
		}

	}
}

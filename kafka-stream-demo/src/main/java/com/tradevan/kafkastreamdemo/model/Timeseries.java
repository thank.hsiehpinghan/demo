package com.tradevan.kafkastreamdemo.model;

import com.tradevan.kafkastreamdemo.model.Order.Product;

public class Timeseries {
	private Integer value_0 = 0;
	private Integer value_1 = 0;

	public Timeseries() {
		super();
	}

	public Timeseries(Integer value_0, Integer value_1) {
		super();
		this.value_0 = value_0;
		this.value_1 = value_1;
	}

	public Integer getValue_0() {
		return value_0;
	}

	public void setValue_0(Integer value_0) {
		this.value_0 = value_0;
	}

	public Integer getValue_1() {
		return value_1;
	}

	public void setValue_1(Integer value_1) {
		this.value_1 = value_1;
	}

//	public Timeseries add(Order order) {
//		for (Product product : order.getProducts()) {
//			value_0 += order.getProducts().size();
//			value_1 += product.getAmount() * product.getPrice();
//		}
//		return this;
//	}

	public Timeseries add(Timeseries timeseries) {
		value_0 += timeseries.getValue_0();
		value_1 += timeseries.getValue_1();
		return this;
	}

	public static Builder builder(Order order) {
		return new Builder(order);
	}

	public static final class Builder {
		private Integer value_0;
		private Integer value_1;

		public Builder(Order order) {
			super();
			value_0 = order.getProducts().size();
			value_1 = 0;
			for (Product product : order.getProducts()) {
				value_1 += product.getAmount() * product.getPrice();
			}
		}

		public Timeseries build() {
			return new Timeseries(value_0, value_1);
		}

	}
}

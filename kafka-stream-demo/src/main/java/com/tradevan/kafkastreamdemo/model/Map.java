package com.tradevan.kafkastreamdemo.model;

public class Map {
	private Double latitude;
	private Double longitude;

	public Map() {
		super();
	}

	public Map(Double latitude, Double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public static Builder builder(Order order) {
		return new Builder(order);
	}

	public static final class Builder {
		private Double latitude;
		private Double longitude;

		public Builder(Order order) {
			super();
			this.latitude = order.getLatitude();
			this.longitude = order.getLongitude();
		}

		public Map build() {
			return new Map(latitude, longitude);
		}

	}
}

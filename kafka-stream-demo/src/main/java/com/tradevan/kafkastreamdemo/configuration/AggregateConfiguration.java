package com.tradevan.kafkastreamdemo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

import com.tradevan.kafkastreamdemo.stream.AggregateStream;

@Configuration
@Profile("aggregate")
public class AggregateConfiguration {
	@Autowired
	private AggregateStream aggregateStream;

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		aggregateStream.start();
	}

}

package com.tradevan.kafkastreamdemo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

import com.tradevan.kafkastreamdemo.stream.SimpleStream;

@Configuration
@Profile("simple")
public class SimpleStreamConfiguration {
	@Autowired
	private SimpleStream simpleStream;

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		simpleStream.start();
	}

}

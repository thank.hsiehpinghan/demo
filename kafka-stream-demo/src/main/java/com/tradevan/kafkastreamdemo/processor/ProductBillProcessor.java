package com.tradevan.kafkastreamdemo.processor;

import java.util.Date;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.AbstractProcessor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;

import com.tradevan.kafkastreamdemo.model.Order;
import com.tradevan.kafkastreamdemo.model.Order.Product;
import com.tradevan.kafkastreamdemo.model.ProductBill;

public class ProductBillProcessor extends AbstractProcessor<Long, Order> {
	private static final long PUNCTUATIONS_INTERVAL_MILLI_SECOND = 1000;
	private ProcessorContext context;
	private KeyValueStore<String, ProductBill> productBillStore;

	@Override
	@SuppressWarnings("unchecked")
	public void init(ProcessorContext context) {
		this.context = context;
		this.context.schedule(PUNCTUATIONS_INTERVAL_MILLI_SECOND);
		productBillStore = (KeyValueStore<String, ProductBill>) this.context.getStateStore("product_bill_store");
	}

	public void process(Long key, Order order) {
		for (Product product : order.getProducts()) {
			Integer value = product.getAmount() * product.getPrice();
			String name = product.getName();
			ProductBill productBill = new ProductBill(value, name);
			ProductBill beforeProductBill = productBillStore.get(name);
			if (beforeProductBill != null) {
				productBill.addValue(beforeProductBill.getValue());
			}
			productBillStore.put(name, productBill);
		}
		this.context.commit();
	}

	@Override
	public void punctuate(long timestamp) {
		KeyValueIterator<String, ProductBill> iter = productBillStore.all();
		while (iter.hasNext()) {
			KeyValue<String, ProductBill> kv = iter.next();
			Date now = new Date();
			this.context.forward(Long.valueOf(now.getTime()), kv.value);
		}
	}

}
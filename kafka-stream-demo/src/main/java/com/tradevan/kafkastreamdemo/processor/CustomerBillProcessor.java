package com.tradevan.kafkastreamdemo.processor;

import java.util.Date;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.AbstractProcessor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;

import com.tradevan.kafkastreamdemo.model.CustomerBill;
import com.tradevan.kafkastreamdemo.model.Order;

public class CustomerBillProcessor extends AbstractProcessor<Long, Order> {
	private static final long PUNCTUATIONS_INTERVAL_MILLI_SECOND = 1000;
	private ProcessorContext context;
	private KeyValueStore<String, CustomerBill> customerBillStore;

	@Override
	@SuppressWarnings("unchecked")
	public void init(ProcessorContext context) {
		this.context = context;
		this.context.schedule(PUNCTUATIONS_INTERVAL_MILLI_SECOND);
		customerBillStore = (KeyValueStore<String, CustomerBill>) this.context.getStateStore("customer_bill_store");
	}

	public void process(Long key, Order order) {
		CustomerBill customerBill = CustomerBill.builder(order).build();
		String customer = customerBill.getName();
		CustomerBill beforeCustomerBill = customerBillStore.get(customer);
		if (beforeCustomerBill != null) {
			customerBill.addValue(beforeCustomerBill.getValue());
		}
		customerBillStore.put(customer, customerBill);
		this.context.commit();
	}

	@Override
	public void punctuate(long timestamp) {
		KeyValueIterator<String, CustomerBill> iter = customerBillStore.all();
		while (iter.hasNext()) {
			KeyValue<String, CustomerBill> kv = iter.next();
			Date now = new Date();
			this.context.forward(Long.valueOf(now.getTime()), kv.value);
		}
	}

}